package eu.proszowski.surreptitiousknell.meaningsfinder

import java.util.stream.Collectors


class WordsGenerator {

    static void generate() {
        String loremIpsum = "One year ago I left my home for school and never returned. I was shot by a Taliban bullet and was\n" +
                "flown out of Pakistan unconscious. Some people say I will never return home but I believe firmly in\n" +
                "my heart that I will. To be torn from the country that you love is not something to wish on anyone.\n" +
                "Now, every morning when I open my eyes, I long to see my old room full of my things, my clothes\n" +
                "all over the floor and my school prizes on the shelves. Instead I am in a country which is five hours\n" +
                "behind my beloved homeland Pakistan and my home in the Swat Valley. But my country is centuries\n" +
                "behind this one. Here there is any convenience you can imagine. Water running from every tap, hot or\n" +
                "cold as you wish; lights at the flick of a switch, day and night, no need for oil lamps; ovens to cook on\n" +
                "that don’t need anyone to go and fetch gas cylinders from the bazaar. Here everything is so modern\n" +
                "one can even find food ready cooked in packets.\n" +
                "When I stand in front of my window and look out, I see tall buildings, long roads full of vehicles\n" +
                "moving in orderly lines, neat green hedges and lawns, and tidy pavements to walk on. I close my eyes\n" +
                "and for a moment I am back in my valley – the high snow-topped mountains, green waving fields\n" +
                "and fresh blue rivers – and my heart smiles when it looks at the people of Swat. My mind transports\n" +
                "me back to my school and there I am reunited with my friends and teachers. I meet my best friend\n" +
                "Moniba and we sit together, talking and joking as if I had never left.\n" +
                "Then I remember I am in Birmingham, England.\n" +
                "The day when everything changed was Tuesday, 9 October 2012. It wasn’t the best of days to start\n" +
                "with as it was the middle of school exams, though as a bookish girl I didn’t mind them as much as\n" +
                "some of my classmates.\n" +
                "That morning we arrived in the narrow mud lane off Haji Baba Road in our usual procession of\n" +
                "brightly painted rickshaws, sputtering diesel fumes, each one crammed with five or six girls. Since\n" +
                "the time of the Taliban our school has had no sign and the ornamented brass door in a white wall\n" +
                "across from the woodcutter’s yard gives no hint of what lies beyond.\n" +
                "For us girls that doorway was like a magical entrance to our own special world. As we skipped\n" +
                "through, we cast off our head-scarves like winds puffing away clouds to make way for the sun then\n" +
                "ran helter-skelter up the steps. At the top of the steps was an open courtyard with doors to all the\n" +
                "classrooms. We dumped our backpacks in our rooms then gathered for morning assembly under the\n" +
                "sky, our backs to the mountains as we stood to attention. One girl commanded, ‘Assaan bash! ’ or\n" +
                "‘Stand at ease!’ and we clicked our heels and responded,";
        final String transformed = loremIpsum
                .replace(",", "")
                .replace("\n", " ")
                .replace(".", "")
                .replace("'", "")
                .replace("!", "")
                .replace("‘", "")
                .toLowerCase();
        Arrays.stream(transformed.split(" ")).collect(Collectors.toSet()).each{
            System.out.print("\"" + it + "\", ");
        };
    }

    static void main(String[] args) {
        generate()
    }
}
