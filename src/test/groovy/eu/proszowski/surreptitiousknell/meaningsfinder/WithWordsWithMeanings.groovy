package eu.proszowski.surreptitiousknell.meaningsfinder


trait WithWordsWithMeanings {

    WordId DUMMY_ID = WordId.EMPTY

    Map<Word, Collection<Meaning>> oneWordWithFewMeanings() {
        Word word = Word.builder().value('simple').build()
        Set<Meaning> meanings = [Meaning.builder().value("Of very low intelligence.").build(),
                                 Meaning.builder().value("A medicinal herb, or a medicine made from one.").build(),
                                 Meaning.builder().value("Composed of a single element; not compound.").build(),
                                 Meaning.builder().value("Used to convey that something is very straightforward.").build(),
                                 Meaning.builder().value("Easily understood or done; presenting no difficulty.").build(),
                                 Meaning.builder().value("Plain, basic, or uncomplicated in form, nature, or design; without much decoration or ornamentation.").build()];

        return [(word): meanings]
    }

    Map<Word, Collection<Meaning>> oneWordWithFewMeaningsAndOneWordWithoutAnyMeaning() {
        LinkedHashMap<Word, Set<Meaning>> wordsWithMeanings = oneWordWithFewMeanings()
        Word wordWithoutMeaning = Word.builder().value("fdsajkofsdjaifdsfdsfdsfsdfdsf").build()
        wordsWithMeanings.put(wordWithoutMeaning, [] as Set)
        return wordsWithMeanings
    }

    Map<Word, Collection<Meaning>> fewWordsWithMeanings() {
        List<WordWithMeanings> wordsWithMeanings = [
                WordWithMeanings.builder()
                        .word(Word.builder().value("simple").build())
                        .meanings([Meaning.builder()
                                           .value('DUMMY')
                                           .build()])
                        .build(),
                WordWithMeanings.builder()
                        .word(Word.builder().value("easy").build())
                        .meanings([Meaning.builder()
                                           .value('DUMMY')
                                           .build()])
                        .build(),
                WordWithMeanings.builder()
                        .word(Word.builder().value("peasy").build())
                        .meanings([Meaning.builder()
                                           .value('DUMMY')
                                           .build()])
                        .build(),
                WordWithMeanings.builder()
                        .word(Word.builder().value("lemon").build())
                        .meanings([Meaning.builder()
                                           .value('few')
                                           .build(),
                                   Meaning.builder()
                                           .value('meanings')
                                           .build()])
                        .build(),
                WordWithMeanings.builder()
                        .word(Word.builder().value("squeezy").build())
                        .meanings([Meaning.builder()
                                           .value('MEANING')
                                           .build()])
                        .build(),
        ]

        return wordsWithMeanings.collectEntries{
            [(it.word) : it.meanings]
        }
    }

    Map<Word, Collection<Meaning>> wordForWhichEmptyObjectOccuredWithMeanigns() {
        Collection<Meaning> meanings = [Meaning.builder().value("A very long time; ages.").build(), Meaning.builder().value("The time taken by the earth to make one revolution around the sun.").build(), Meaning.builder().value("One's age or time of life.").build(), Meaning.builder().value("A set of students grouped together as being of roughly similar ages, mostly entering a school or college in the same academic year.").build()]
        return [(Word.builder().value("year").build()) : meanings]

    }

    Map<Word, Collection<Meaning>> fewWordsWithMeaningsAndFewWithEmptyMeanings() {
        Map<Word, Collection<Meaning>> wordsWithMeanings = fewWordsWithMeanings()
        wordsWithMeanings.put(Word.builder().value("some").build(), [])
        wordsWithMeanings.put(Word.builder().value("values").build(), [])
        wordsWithMeanings.put(Word.builder().value("without").build(), [])
        wordsWithMeanings.put(Word.builder().value("meanings").build(), [])
        return wordsWithMeanings
    }

}