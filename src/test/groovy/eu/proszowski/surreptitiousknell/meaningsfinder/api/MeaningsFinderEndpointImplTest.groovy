package eu.proszowski.surreptitiousknell.meaningsfinder.api

import eu.proszowski.surreptitiousknell.meaningsfinder.Meaning
import eu.proszowski.surreptitiousknell.meaningsfinder.MeaningsFinderFacade
import eu.proszowski.surreptitiousknell.meaningsfinder.WithWordsWithMeanings
import eu.proszowski.surreptitiousknell.meaningsfinder.Word
import spock.lang.Specification

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when


class MeaningsFinderEndpointImplTest extends Specification implements WithWordsWithMeanings {

    void "should return both - recognized and unrecognized words"() {
        given:
        MeaningsFinderFacade meaningsFinderFacade = mock(MeaningsFinderFacade)

        Map<Word, Collection<Meaning>> wordsWithMeanings = fewWordsWithMeaningsAndFewWithEmptyMeanings()

        when(meaningsFinderFacade.findMeaningsForWords(wordsWithMeanings.keySet() as Collection<String>))
                .thenReturn(wordsWithMeanings)

        MeaningsFinderEndpoint meaningsFinderEndpoint = new MeaningsFinderEndpointImpl(meaningsFinderFacade)

        List<WordWithMeaningsDto> wordsWithNonEmptyMeanings = wordsWithMeanings.findAll {
            !it.value.isEmpty()
        }.collect {
            WordWithMeaningsDto.builder()
                    .id(it.key.id.raw)
                    .value(it.key.value)
                    .meanings(it.value.stream().collect { it.value })
                    .build()
        }

        List<String> wordsWithoutMeanings = wordsWithMeanings.findAll {
            it.value.isEmpty()
        }.collect {
            it.key.value
        }

        when:
        ResponseDto responseDto = meaningsFinderEndpoint.getMeaningsForWords(wordsWithMeanings.keySet() as Collection<String>)

        then:
        responseDto.wordsWithMeanings == wordsWithNonEmptyMeanings
        responseDto.unrecognizedWords == wordsWithoutMeanings

    }
}
