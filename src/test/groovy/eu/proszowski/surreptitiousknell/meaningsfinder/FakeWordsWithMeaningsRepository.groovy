package eu.proszowski.surreptitiousknell.meaningsfinder


class FakeWordsWithMeaningsRepository implements WordsWithMeaningsRepository {

    private final Map<Word, Collection<Meaning>> wordsWithMeanings = [:];

    @Override
    Word save(final Word key, final Collection<Meaning> value) {
        Word wordWithId = Word.builder()
                .id(WordId.builder().raw(UUID.randomUUID().toString()).build())
                .value(key.value)
                .build()
        wordsWithMeanings.put(wordWithId, value)
        return wordWithId
    }

    @Override
    Map<Word, Set<Meaning>> findByValues(final Collection<String> words) {
        wordsWithMeanings.entrySet().findAll{
            words.contains(it.key.value)
        }.collectEntries{
            [(it.key): it.value]
        }
    }
}
