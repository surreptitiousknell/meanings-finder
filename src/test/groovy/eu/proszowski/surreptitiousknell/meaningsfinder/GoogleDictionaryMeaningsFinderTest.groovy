package eu.proszowski.surreptitiousknell.meaningsfinder

import eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary.GoogleDictionaryMeaningsFinder
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.anyString
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when


class GoogleDictionaryMeaningsFinderTest extends Specification implements WithWordsWithMeanings, WithJsonResponses {


    void "should return meanings for given words"(Map<Word, Set<Meaning>> expectedWordsWithMeanings) {
        given:
        RestTemplate restTemplate = new RestTemplate()
        MeaningsFinder meaningsFinder = GoogleDictionaryMeaningsFinder.builder().restTemplate(restTemplate).build()
        Set<Word> words = expectedWordsWithMeanings.keySet()

        when:
        Map<Word, Set<Meaning>> actualWordsWithMeanings = words.collectEntries{
            [(it) : meaningsFinder.findMeaningsForWord(it)]
        }

        then:
        actualWordsWithMeanings == expectedWordsWithMeanings

        where:
        expectedWordsWithMeanings << [oneWordWithFewMeanings() ]
    }

    void "should return empty set for words which could not be found"(Map<Word, Set<Meaning>> expectedWordsWithMeanings) {
        given:
        RestTemplate restTemplate = new RestTemplate()
        MeaningsFinder meaningsFinder = GoogleDictionaryMeaningsFinder.builder().restTemplate(restTemplate).build()
        Set<Word> words = expectedWordsWithMeanings.keySet()

        when:
        Map<Word, Set<Meaning>> actualWordsWithMeanings = words.collectEntries{
            [(it) : meaningsFinder.findMeaningsForWord(it)]
        }

        then:
        actualWordsWithMeanings == expectedWordsWithMeanings

        where:
        expectedWordsWithMeanings << [ oneWordWithFewMeaningsAndOneWordWithoutAnyMeaning() ]
    }

    void "should ignore empty objects"(Map<Word, Set<Meaning>> expectedWordsWithMeanings){
        given:
        RestTemplate restTemplate = mock(RestTemplate)
        when(restTemplate.getForEntity(anyString(), any(Class))).thenReturn(ResponseEntity.of(Optional.of(jsonResponseWithEmptyObject())))
        MeaningsFinder meaningsFinder = GoogleDictionaryMeaningsFinder.builder().restTemplate(restTemplate).build()

        Set<Word> words = expectedWordsWithMeanings.keySet()

        when:
        Map<Word, Set<Meaning>> actualWordsWithMeanings = words.collectEntries{
            [(it) : meaningsFinder.findMeaningsForWord(it)]
        }

        then:
        actualWordsWithMeanings.every {
            Set<Meaning> meanings = expectedWordsWithMeanings.get(it.key)
            meanings == it.value
        }

        where:
        expectedWordsWithMeanings << [wordForWhichEmptyObjectOccuredWithMeanigns() ]
    }

}
