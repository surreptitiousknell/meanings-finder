package eu.proszowski.surreptitiousknell.meaningsfinder

import groovy.transform.Canonical


@Canonical
class FakeMeaningsFinder implements MeaningsFinder {

    private Map<Word, Collection<Meaning>> wordsWithMeanings;

    static FakeMeaningsFinder withWordsWithMeanings(Map<Word, Collection<Meaning>> wordsWithMeanings) {
        return new FakeMeaningsFinder(wordsWithMeanings: wordsWithMeanings)
    }

    @Override
    Collection<Meaning> findMeaningsForWord(final Word word) {
        return wordsWithMeanings.get(word) ?: []
    }

    void without(final Map<Word, Collection<Meaning>> wordsWithMeaningsToRemove) {
        wordsWithMeanings = wordsWithMeaningsToRemove.findAll {
            !wordsWithMeanings.containsKey(it.key)
        }
    }
}
