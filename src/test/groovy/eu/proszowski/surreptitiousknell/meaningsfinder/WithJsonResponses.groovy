package eu.proszowski.surreptitiousknell.meaningsfinder

import com.fasterxml.jackson.databind.ObjectMapper
import eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary.GoogleDictionaryResponse


trait WithJsonResponses {


    GoogleDictionaryResponse[] jsonResponseWithEmptyObject() {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(jsonResponseWithEmptyObjectString(), GoogleDictionaryResponse[])
    }

    String jsonResponseWithEmptyObjectString() {
        "[\n" +
                "  {\n" +
                "    \"word\": \"year\",\n" +
                "    \"phonetic\": \"/jɪə/\",\n" +
                "    \"origin\": \"Old English gē(a)r, of Germanic origin; related to Dutch jaar and German Jahr, from an Indo-European root shared by Greek hōra ‘season’.\",\n" +
                "    \"meaning\": {\n" +
                "      \"noun\": [\n" +
                "        {\n" +
                "          \"definition\": \"The time taken by the earth to make one revolution around the sun.\"\n" +
                "        },\n" +
                "        {},\n" +
                "        {\n" +
                "          \"definition\": \"One's age or time of life.\",\n" +
                "          \"example\": \"she had a composure well beyond her years\",\n" +
                "          \"synonyms\": [\n" +
                "            \"number of years\",\n" +
                "            \"lifetime\",\n" +
                "            \"duration\",\n" +
                "            \"length of life\"\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"definition\": \"A very long time; ages.\",\n" +
                "          \"example\": \"it's going to take years to put that right\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"definition\": \"A set of students grouped together as being of roughly similar ages, mostly entering a school or college in the same academic year.\",\n" +
                "          \"example\": \"most of the girls in my year were leaving at the end of the term\",\n" +
                "          \"synonyms\": [\n" +
                "            \"class\",\n" +
                "            \"form\",\n" +
                "            \"study group\",\n" +
                "            \"school group\",\n" +
                "            \"set\",\n" +
                "            \"stream\",\n" +
                "            \"band\"\n" +
                "          ]\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  }\n" +
                "]"
    }
}