package eu.proszowski.surreptitiousknell.meaningsfinder

import spock.lang.Specification

class MeaningsFinderFacadeTest extends Specification implements WithWordsWithMeanings, WithSetup {

    void "should find meanings for given words in the internet"() {
        given:
        Map<Word, Collection<Meaning>> expectedWordsWithMeanings = fewWordsWithMeanings()
        thereAreWordsWithMeaningsInTheInternet(expectedWordsWithMeanings)

        when:
        Map<Word, Collection<Meaning>> actualWordsWithMeanings = meaningsFinderFacade.findMeaningsForWords(expectedWordsWithMeanings.collect { it.key.value })

        then:
        actualWordsWithMeanings == expectedWordsWithMeanings
    }

    void "should save words fetched from the internet in database"(){
        given:
        Map<Word, Collection<Meaning>> expectedWordsWithMeanings = fewWordsWithMeanings()
        thereAreWordsWithMeaningsInTheInternet(expectedWordsWithMeanings)

        when:
        meaningsFinderFacade.findMeaningsForWords(expectedWordsWithMeanings.collect { it.key.value })
        wordsInTheInternetDisappear(expectedWordsWithMeanings)
        Map<Word, Collection<Meaning>> actualWordsWithMeanings = meaningsFinderFacade.findMeaningsForWords(expectedWordsWithMeanings.collect { it.key.value })

        then:
        actualWordsWithMeanings.values().containsAll(expectedWordsWithMeanings.values())
        actualWordsWithMeanings.keySet().every{
            it.getId() != null
        }
    }
}
