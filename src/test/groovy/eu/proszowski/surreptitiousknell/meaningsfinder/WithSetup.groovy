package eu.proszowski.surreptitiousknell.meaningsfinder


trait WithSetup {
    MeaningsFinderFacade  meaningsFinderFacade
    FakeMeaningsFinder meaningsFinder
    FakeWordsWithMeaningsRepository wordsWithMeaningsRepository;

    void thereAreWordsWithMeaningsInTheInternet(Map<Word, Collection<Meaning>> wordsWithMeanings){
        meaningsFinder = FakeMeaningsFinder.withWordsWithMeanings(wordsWithMeanings)
        wordsWithMeaningsRepository = new FakeWordsWithMeaningsRepository()
        meaningsFinderFacade = MeaningsFinderFacade.builder()
                .meaningsFinder(meaningsFinder)
                .wordsWithMeaningsRepository(wordsWithMeaningsRepository)
                .build()
    }

    void wordsInTheInternetDisappear(Map<Word, Collection<Meaning>> wordsWithMeanings) {
        meaningsFinder.without(wordsWithMeanings)
    }
}