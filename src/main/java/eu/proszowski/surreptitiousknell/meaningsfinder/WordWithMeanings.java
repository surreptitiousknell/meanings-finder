package eu.proszowski.surreptitiousknell.meaningsfinder;

import lombok.Builder;
import lombok.Value;
import java.util.Collection;

@Value
@Builder
class WordWithMeanings {
    private Word word;
    private Collection<Meaning> meanings;
}
