package eu.proszowski.surreptitiousknell.meaningsfinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeaningsFinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeaningsFinderApplication.class, args);
	}

}
