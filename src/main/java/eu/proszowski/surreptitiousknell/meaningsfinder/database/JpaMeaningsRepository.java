package eu.proszowski.surreptitiousknell.meaningsfinder.database;

import org.springframework.data.repository.CrudRepository;

public interface JpaMeaningsRepository extends CrudRepository<MeaningEntity, String> {
}
