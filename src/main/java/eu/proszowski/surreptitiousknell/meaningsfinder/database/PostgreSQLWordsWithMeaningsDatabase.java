package eu.proszowski.surreptitiousknell.meaningsfinder.database;

import eu.proszowski.surreptitiousknell.meaningsfinder.Meaning;
import eu.proszowski.surreptitiousknell.meaningsfinder.Word;
import eu.proszowski.surreptitiousknell.meaningsfinder.WordId;
import eu.proszowski.surreptitiousknell.meaningsfinder.WordsWithMeaningsRepository;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class PostgreSQLWordsWithMeaningsDatabase implements WordsWithMeaningsRepository {

    private final JpaWordsRepository jpaWordsRepository;

    private final JpaMeaningsRepository jpaMeaningsRepository;

    public PostgreSQLWordsWithMeaningsDatabase(final JpaWordsRepository jpaWordsRepository, final JpaMeaningsRepository jpaMeaningsRepository) {
        this.jpaWordsRepository = jpaWordsRepository;
        this.jpaMeaningsRepository = jpaMeaningsRepository;
    }

    @Override
    public Word save(final Word key, final Collection<Meaning> value) {
        final WordEntity wordEntity = new WordEntity();
        final Set<MeaningEntity> meanings = value.stream().map(meaning -> new MeaningEntity(null, meaning.getValue())).collect(Collectors.toSet());

        wordEntity.setValue(key.getValue());
        wordEntity.setMeanings(meanings);

        final WordEntity savedEntity = jpaWordsRepository.save(wordEntity);
        return Word.builder()
                .value(savedEntity.value)
                .id(WordId.builder().raw(savedEntity.id.toString()).build())
                .build();
    }

    @Override
    public Map<Word, Set<Meaning>> findByValues(final Collection<String> words) {
        return words.stream()
                .map(jpaWordsRepository::findByValue)
                .filter(Objects::nonNull)
                .collect(mapEntitiesToModel());
    }

    private Collector<WordEntity, ?, Map<Word, Set<Meaning>>> mapEntitiesToModel() {
        return Collectors.toMap(
                e -> Word.builder()
                        .id(WordId.builder()
                                .raw(e.id != null ? e.id.toString() : null)
                                .build())
                        .value(e.value)
                        .build(),
                e -> e.meanings.stream()
                        .map(f -> Meaning.builder()
                                .value(f.value)
                                .build())
                        .collect(Collectors.toSet())
        );
    }
}
