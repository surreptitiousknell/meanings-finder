package eu.proszowski.surreptitiousknell.meaningsfinder.database;

import org.springframework.data.repository.CrudRepository;

public interface JpaWordsRepository extends CrudRepository<WordEntity, String> {
    WordEntity findByValue(String word);
}
