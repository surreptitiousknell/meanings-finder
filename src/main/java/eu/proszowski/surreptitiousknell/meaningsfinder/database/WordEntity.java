package eu.proszowski.surreptitiousknell.meaningsfinder.database;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "words")
@Data
public class WordEntity {

    @Id
    @GeneratedValue(generator = "UUID", strategy = GenerationType.AUTO)
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "word_id", updatable = false, nullable = false)
    UUID id;

    @Column(name = "word", updatable = false, nullable = false)
    String value;

    @OneToMany(cascade = CascadeType.ALL)
    Set<MeaningEntity> meanings;

}
