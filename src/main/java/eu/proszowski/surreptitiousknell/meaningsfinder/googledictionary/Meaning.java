package eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary;

import lombok.Data;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
public class Meaning {

    private List<Adjective> adjective = null;
    private List<Noun> noun = null;
    private List<Exclamation> exclamation = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}
