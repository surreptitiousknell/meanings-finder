package eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary;

import lombok.Data;
import java.util.HashMap;
import java.util.Map;

@Data
public class GoogleDictionaryResponse {

    private String word;
    private String phonetic;
    private String origin;
    private Meaning meaning;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

}
