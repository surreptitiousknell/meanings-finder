package eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary;

public interface WithDefinition {
    String getDefinition();
}
