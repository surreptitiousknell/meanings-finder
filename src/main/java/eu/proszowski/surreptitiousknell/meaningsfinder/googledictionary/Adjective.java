package eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary;

import lombok.Data;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
class Adjective implements WithDefinition {

    private String definition;
    private String example;
    private List<String> synonyms = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}
