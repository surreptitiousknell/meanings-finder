package eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary;

import eu.proszowski.surreptitiousknell.meaningsfinder.Meaning;
import eu.proszowski.surreptitiousknell.meaningsfinder.MeaningsFinder;
import eu.proszowski.surreptitiousknell.meaningsfinder.Word;
import lombok.Builder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Builder
public class GoogleDictionaryMeaningsFinder implements MeaningsFinder {
    private final RestTemplate restTemplate;

    @Override
    public Set<eu.proszowski.surreptitiousknell.meaningsfinder.Meaning> findMeaningsForWord(final Word word) {
        final ResponseEntity<GoogleDictionaryResponse[]> response;
        try {
            response = restTemplate.getForEntity(String.format("https://googledictionaryapi.eu-gb.mybluemix.net/?define=%s&lang=en", word.getValue()), GoogleDictionaryResponse[].class);
        } catch (final HttpClientErrorException e) {
            return Collections.emptySet();
        }

        return Arrays.stream(response.getBody())
                .map(GoogleDictionaryResponse::getMeaning)
                .flatMap(meaning -> Stream.of(meaning.getAdjective(), meaning.getNoun(), meaning.getExclamation()))
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(WithDefinition::getDefinition)
                .filter(Objects::nonNull)
                .map(definition -> Meaning.builder().value(definition).build())
                .collect(Collectors.toSet());
    }
}
