package eu.proszowski.surreptitiousknell.meaningsfinder;

import eu.proszowski.surreptitiousknell.meaningsfinder.database.JpaMeaningsRepository;
import eu.proszowski.surreptitiousknell.meaningsfinder.database.JpaWordsRepository;
import eu.proszowski.surreptitiousknell.meaningsfinder.database.PostgreSQLWordsWithMeaningsDatabase;
import eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary.GoogleDictionaryMeaningsFinder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {

    @Bean
    MeaningsFinderFacade meaningsFinderFacade(final MeaningsFinder meaningsFinder, final WordsWithMeaningsRepository wordsWithMeaningsRepository) {
        return MeaningsFinderFacade.builder().
                meaningsFinder(meaningsFinder)
                .wordsWithMeaningsRepository(wordsWithMeaningsRepository)
                .build();
    }

    @Bean
    WordsWithMeaningsRepository wordsWithMeaningsRepository(final JpaWordsRepository jpaWordsRepository, JpaMeaningsRepository jpaMeaningsRepository){
        return new PostgreSQLWordsWithMeaningsDatabase(jpaWordsRepository, jpaMeaningsRepository);
    }

    @Bean
    MeaningsFinder meaningsFinder(final RestTemplate restTemplate) {
        return GoogleDictionaryMeaningsFinder.builder()
                .restTemplate(restTemplate)
                .build();
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
