package eu.proszowski.surreptitiousknell.meaningsfinder;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Builder
@Slf4j
public class MeaningsFinderFacade {

    private final MeaningsFinder meaningsFinder;

    private final WordsWithMeaningsRepository wordsWithMeaningsRepository;

    MeaningsFinderFacade(final MeaningsFinder meaningsFinder, final WordsWithMeaningsRepository wordsWithMeaningsRepository) {
        this.meaningsFinder = meaningsFinder;
        this.wordsWithMeaningsRepository = wordsWithMeaningsRepository;
    }

    public Map<Word, Collection<Meaning>> findMeaningsForWords(final Collection<String> words) {
        final Executor executor = Executors.newFixedThreadPool(40);

        final Map<Word, Set<Meaning>> wordsWithMeaningsFromDatabase = wordsWithMeaningsRepository.findByValues(words);

        final List<CompletableFuture<WordWithMeanings>> futures = words.stream()
                .filter(findWordsNotFoundInDatabase(wordsWithMeaningsFromDatabase))
                .map(word ->
                        CompletableFuture.supplyAsync(() -> {
                            log.info("Start looking for word '{}'", word);
                            final Instant before = Instant.now();
                            final Collection<Meaning> meanings = meaningsFinder.findMeaningsForWord(Word.builder().value(word).build());
                            log.info("Found word '{}' in {} ms", word, Duration.between(before, Instant.now()).toMillis());
                            return WordWithMeanings.builder()
                                    .word(Word.builder().value(word).build())
                                    .meanings(meanings)
                                    .build();
                        }, executor)
                ).collect(Collectors.toList());

        final CompletableFuture<Void> allDoneFutures =
                CompletableFuture.allOf(futures.toArray(CompletableFuture[]::new));

        final Map<Word, Collection<Meaning>> wordsWithMeaningsFromInternet = allDoneFutures
                .thenApply(aVoid -> futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toMap(WordWithMeanings::getWord, WordWithMeanings::getMeanings)))
                .join();

        wordsWithMeaningsFromInternet.entrySet().stream()
                .filter(entry -> !entry.getValue().isEmpty())
                .collect(Collectors.toMap(e -> wordsWithMeaningsRepository.save(e.getKey(), e.getValue()), Map.Entry::getValue));

        return Stream.concat(wordsWithMeaningsFromDatabase.entrySet().stream(), wordsWithMeaningsFromInternet.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    }

    private Predicate<String> findWordsNotFoundInDatabase(final Map<Word, Set<Meaning>> wordsWithMeaningsFromDatabase) {
        return word -> !wordsWithMeaningsFromDatabase.keySet()
                .stream()
                .map(Word::getValue)
                .collect(Collectors.toSet())
                .contains(word);
    }
}

