package eu.proszowski.surreptitiousknell.meaningsfinder;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface WordsWithMeaningsRepository {
    Word save(Word key, Collection<Meaning> value);

    Map<Word, Set<Meaning>> findByValues(Collection<String> words);
}
