package eu.proszowski.surreptitiousknell.meaningsfinder;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class WordId {
    static final WordId EMPTY = WordId.builder().raw("EMPTY").build();
    private String raw;
}
