package eu.proszowski.surreptitiousknell.meaningsfinder;

import lombok.Builder;

@Builder
public class WordWithId {
    private Word word;
}
