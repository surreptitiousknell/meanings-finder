package eu.proszowski.surreptitiousknell.meaningsfinder;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class Meaning {

    @NonNull
    private String value;
}
