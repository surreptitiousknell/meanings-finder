package eu.proszowski.surreptitiousknell.meaningsfinder;

import java.util.Collection;

public interface MeaningsFinder {

    Collection<Meaning> findMeaningsForWord(Word word);
}
