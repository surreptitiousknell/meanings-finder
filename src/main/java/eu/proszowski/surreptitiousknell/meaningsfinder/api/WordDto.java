package eu.proszowski.surreptitiousknell.meaningsfinder.api;

import eu.proszowski.surreptitiousknell.meaningsfinder.Word;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WordDto {
    String id;
    String value;

    static WordDto fromWord(final Word key) {
        return WordDto.builder()
                .value(key.getValue())
                .build();
    }
}
