package eu.proszowski.surreptitiousknell.meaningsfinder.api;

import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@Builder
class ResponseDto {
    List<WordWithMeaningsDto> wordsWithMeanings;
    List<String> unrecognizedWords;
}
