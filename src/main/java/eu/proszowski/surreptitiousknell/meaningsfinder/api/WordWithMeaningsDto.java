package eu.proszowski.surreptitiousknell.meaningsfinder.api;

import eu.proszowski.surreptitiousknell.meaningsfinder.googledictionary.Meaning;
import lombok.Builder;
import lombok.Data;
import java.util.List;

@Builder
@Data
public class WordWithMeaningsDto {
    private String id;
    private String value;
    private List<String> meanings;
}
