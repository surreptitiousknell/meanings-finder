package eu.proszowski.surreptitiousknell.meaningsfinder.api;

import eu.proszowski.surreptitiousknell.meaningsfinder.Meaning;
import eu.proszowski.surreptitiousknell.meaningsfinder.MeaningsFinderFacade;
import org.springframework.stereotype.Component;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MeaningsFinderEndpointImpl implements MeaningsFinderEndpoint {

    private MeaningsFinderFacade meaningsFinderFacade;

    MeaningsFinderEndpointImpl(MeaningsFinderFacade meaningsFinderFacade) {
        this.meaningsFinderFacade = meaningsFinderFacade;
    }

    @Override
    public ResponseDto getMeaningsForWords(final Collection<String> words) {

        final List<WordWithMeaningsDto> wordsWitMeanings = meaningsFinderFacade.findMeaningsForWords(words)
                .entrySet()
                .stream()
                .map(entry -> WordWithMeaningsDto.builder()
                        .id(entry.getKey().getId().getRaw())
                        .value(entry.getKey().getValue())
                        .meanings(entry.getValue()
                                .stream()
                                .map(Meaning::getValue)
                                .collect(Collectors.toList()))
                        .build())
                .collect(Collectors.toList());

        final List<WordWithMeaningsDto> wordsWithNonEmptyMeanings = wordsWitMeanings.stream()
                .filter(wordWithMeanings -> !wordWithMeanings.getMeanings().isEmpty())
                .collect(Collectors.toList());

        final List<String> unrecognizedWords = wordsWitMeanings.stream()
                .filter(wordsWithMeanings -> wordsWithMeanings.getMeanings().isEmpty())
                .map(WordWithMeaningsDto::getValue)
                .collect(Collectors.toList());

        return ResponseDto.builder()
                .wordsWithMeanings(wordsWithNonEmptyMeanings)
                .unrecognizedWords(unrecognizedWords)
                .build();
    }

}
