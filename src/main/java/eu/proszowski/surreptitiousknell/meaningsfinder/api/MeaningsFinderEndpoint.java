package eu.proszowski.surreptitiousknell.meaningsfinder.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.Collection;

@RestController
public interface MeaningsFinderEndpoint {

    @PostMapping("findMeaningsForWords")
    ResponseDto getMeaningsForWords(@RequestBody  Collection<String> words);

}
