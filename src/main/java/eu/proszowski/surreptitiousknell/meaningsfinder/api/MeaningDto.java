package eu.proszowski.surreptitiousknell.meaningsfinder.api;

import eu.proszowski.surreptitiousknell.meaningsfinder.Meaning;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
class MeaningDto {
    private String value;

    static MeaningDto fromMeaning(final Meaning meaning) {
        return MeaningDto.builder()
                .value(meaning.getValue())
                .build();
    }
}
