package eu.proszowski.surreptitiousknell.meaningsfinder;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Builder
@Value
public class Word {

    @NonNull
    @Builder.Default
    private WordId id = WordId.EMPTY;

    @NonNull
    private String value;
}
